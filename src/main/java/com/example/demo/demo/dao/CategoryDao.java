package com.example.demo.demo.dao;

import com.example.demo.demo.Model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryDao extends JpaRepository<Category,Integer> {
    Category findByType(String type);
}
