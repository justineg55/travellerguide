package com.example.demo.demo.Controller;

import com.example.demo.demo.Model.City;
import com.example.demo.demo.dao.CityDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
public class CityController {
    private CityDao cityDao;

    @Autowired
    public CityController(CityDao cityDao) {
        this.cityDao = cityDao;
    }

    @GetMapping("/cities/{id}")
    public City getCity(@PathVariable int id) {
        return cityDao.findById(id).orElse(null);
    }

    @GetMapping("/cities")
    public List<City> getCities() {
        return cityDao.findAll();
    }

    @PutMapping("/cities")
    public City saveCity(@RequestBody City city) {
        return cityDao.save(city);
    }
}
